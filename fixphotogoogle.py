import os
import shutil
import json
from datetime import datetime

def copy_files_by_timestamp(root_dir):
    for dirpath, _, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.endswith('.json'):
                json_file_path = os.path.join(dirpath, filename)
                with open(json_file_path, 'r') as json_file:
                    try:
                        data = json.load(json_file)
                        photo_taken_time = data.get('photoTakenTime')
                        if photo_taken_time and 'timestamp' in photo_taken_time:
                            timestamp = float(photo_taken_time['timestamp'])
                            dt = datetime.fromtimestamp(timestamp)
                            target_dir = os.path.join('C:\\GOOGLE', dt.strftime("%Y-%m"))
                            os.makedirs(target_dir, exist_ok=True)
                            target_dir = os.path.join(target_dir, dt.strftime("%Y-%m-%d"))
                            os.makedirs(target_dir, exist_ok=True)
                            title = data.get('title')
                            if title:
                                title_file_path = os.path.join(dirpath, title)
                                if os.path.exists(title_file_path):
                                    shutil.copy2(title_file_path, target_dir)
                                    print(f"Copied {title} to {target_dir}")
                                else:
                                    print(f"File {title} mentioned in {json_file_path} not found.")
                            else:
                                print(f"No 'title' field found in {json_file_path}.")
                    except json.JSONDecodeError as e:
                        print(f"Error decoding JSON file {json_file_path}: {e}")

# Change 'root_directory' to the path where your JSON files are located
root_directory = 'Z:\\Google\\Takeout\\Google Fotos\\Photos from 2023'
copy_files_by_timestamp(root_directory)

