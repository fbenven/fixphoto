import os
from PIL import Image
from datetime import datetime
import shutil
import re

# Função para extrair a data de "Tirada Em" (DateTimeOriginal) do Exif
def get_taken_date(image_path):
    try:
        with Image.open(image_path) as img:
            exif_data = img._getexif()
            if exif_data:
                if 36867 in exif_data:
                    return datetime.strptime(exif_data[36867], "%Y:%m:%d %H:%M:%S")
    except Exception as e:
        print(f"Erro ao ler Exif em {image_path}: {e}")
    return None

# Função para extrair a data do nome do arquivo
def extract_date_from_filename(filename):
    # Use expressão regular para procurar uma data no nome do arquivo
    match = re.search(r'(\d{4}\d{2}\d{2}_)', filename)
    if match:
        return datetime.strptime(match.group(1), "%Y-%m-%d")
    match = re.search(r'IMG-(\d{4}\d{2}\d{2}-)', filename)
    if match:
        return datetime.strptime(match.group(1), "%Y-%m-%d")
    return None

# Diretório de origem e destino
diretorio_origem = 'D:\\Google\\Takeout\\Google Fotos\\Photos from 2023'
diretorio_destino = 'C:\\GOOGLE'

# Percorre todos os arquivos no diretório de origem e subdiretórios
for root, dirs, files in os.walk(diretorio_origem):
    for filename in files:
        if filename.lower().endswith(('.jpg', '.jpeg', '.png', '.gif', '.bmp', '.tiff')):
            origem_arquivo = os.path.join(root, filename)
            data_tirada = get_taken_date(origem_arquivo)
            
            if not data_tirada:
                data_tirada = extract_date_from_filename(filename)
            
            if data_tirada:
                pasta_destino = os.path.join(diretorio_destino, data_tirada.strftime("%Y-%m-%d"))
                os.makedirs(pasta_destino, exist_ok=True)
                destino_arquivo = os.path.join(pasta_destino, filename)
                shutil.copy2(origem_arquivo, destino_arquivo)
                print(f"Arquivo copiado: {origem_arquivo} -> {destino_arquivo}")
            else:
                print(f"Data de 'Tirada Em' não encontrada em {origem_arquivo} e nenhum formato de data válido no nome do arquivo")

print("Concluído.")
